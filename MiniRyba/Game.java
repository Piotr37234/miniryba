package MiniRyba;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.DisplayMode;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.List;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Piotr and Krzysztof
 */
public class Game implements MouseMotionListener, MouseListener, KeyListener, Runnable {

    Point pktMyszki = new Point(1, 1);
    private static final long DEMO_TIME = 10000;  //czas gry 
    //private static final int ELEMENTS = 5;   // ilosc obiektow 
    private ScreenManager screen;
    private Image bgImage;
    //private AnimObject[] fishes;
    ArrayList<AnimObject> GameObjects = new ArrayList<AnimObject>();
    //private ArrayList AnimObject[] GameObjects;
    //private Fish[] fishes;
    private Player player;
    int LICZBA_RYB = 0;

    private static boolean czyDziala = true;

    public synchronized void clearGameObjects() {
        Iterator<AnimObject> s = GameObjects.iterator();

        while (s.hasNext()) {
            AnimObject ob = s.next();
            if (ob.isLive == false) {
                GameObjects.remove(ob);
            }
        }
        s.remove();

    }

    Animation abass;
    Animation aminiRyba;
    Animation azlotaRyba;
    Animation apirania;
    Animation awielopletwa;
    Animation abass3;
    Animation apstrag;
    Animation arozdymka;
    Animation aniebieskaryba;

    public void load() {
        //ladowanie rysunków
        bgImage = loadImage("images/tlo.jpg");
        Image imgPlayer = loadImage("images/bass1.png");
        Image bass2 = loadImage("images/bass2.png");
        Image miniRyba = loadImage("images/miniryba.png");
        Image zlotaRyba = loadImage("images/zlotaryba.png");
        Image pirania = loadImage("images/pirania.png");
        Image wielopletwa = loadImage("images/wielopletwa.png");
        Image bass3 = loadImage("images/bass3.png");
        Image pstrag = loadImage("images/pstrag.png");
        Image rozdymka = loadImage("images/rozdymka.png");
        Image niebieskaRyba = loadImage("images/bonusfish.png");
        abass = new Animation();
        abass.addFrame(bass2, 500);

        aminiRyba = new Animation();
        aminiRyba.addFrame(miniRyba, 500);

        azlotaRyba = new Animation();
        azlotaRyba.addFrame(zlotaRyba, 500);

        apirania = new Animation();
        apirania.addFrame(pirania, 500);

        awielopletwa = new Animation();
        awielopletwa.addFrame(wielopletwa, 500);

        abass3 = new Animation();
        abass3.addFrame(bass3, 500);

        apstrag = new Animation();
        apstrag.addFrame(pstrag, 500);

        arozdymka = new Animation();
        arozdymka.addFrame(rozdymka, 500);

        aniebieskaryba = new Animation();
        aniebieskaryba.addFrame(niebieskaRyba, 500);

        Animation animPlayer = new Animation();
        animPlayer.addFrame(imgPlayer, 500);

        player = new Player(animPlayer, 1, 1, 0, 0);
        GameObjects.add(player);

    }

    public void run() {
        try {
            screen = new ScreenManager();
            //pobranie rozdzielczosci ekranu
            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            DisplayMode displayMode = new DisplayMode(d.width, d.height, 16, 80);
            screen.setFullScreen(displayMode, this, this, this);
            load();
            animationLoop(); //uruchom pentle gry
        } finally {
            screen.restoreScreen();

        }
    }

    public void genObiekt() {

        if (LICZBA_RYB == 0 || new Random().nextInt(90+LICZBA_RYB*5) == 1) {

            double size = new Random().nextDouble();
            int punkty = (int) (size * 100);

            float dx = (float) Math.random() - 0.5f;
            float dy = (float) Math.random() - 0.5f;
            dx = 0.1f;
            dy = 0.2f;

            int los = new Random().nextInt(20);
            
            Fish ryba = null;

            if (los == 1) {
                ryba = new Bass2(abass, 0, 0, dx, dy);
            } else if (los == 2 || los == 3 || los == 4 || los == 5 || los == 16 || los == 18 || los == 19) {
                ryba = new MiniRyba(aminiRyba, 0, 0, dx, dy);
            } else if (los == 6 || los == 7) {
                ryba = new ZlotaRyba(azlotaRyba, 0, 0, dx, dy);
            } else if (los == 8 || los == 9 || los == 10) {
                ryba = new Pirania(apirania, 0, 0, dx, dy);
            } else if (los == 11 || los == 12) {
                ryba = new WieloPletwa(awielopletwa, 0, 0, dx, dy);
            } else if (los == 13) {
                ryba = new Bass3(abass3, 0, 0, dx, dy);
            } else if (los == 14) {
                ryba = new Pstrag(apstrag, 0, 0, dx, dy);
            } else if (los == 15) {
                ryba = new Rozdymka(arozdymka, 0, 0, dx, dy);
            } else if (((Player)GameObjects.get(0)).getPoints() > 1000 && los == 17) {
                ryba = new NiebieskaRyba(aniebieskaryba, 0, 0, dx, dy);
            } else {

                return;
            }

            float x;
            float y;
            int licznik = 0;
            do {
                System.out.println(licznik);
                licznik++;
                x = (float) Math.random() * (screen.getWidth() - GameObjects.get(GameObjects.size() - 1).getWidth());
                y = (float) Math.random() * (screen.getHeight() - GameObjects.get(GameObjects.size() - 1).getHeight());
                ryba.setX(x);
                ryba.setY(y);
            } while (((Fish) GameObjects.get(0)).FishConntactTestCreate(ryba, 0.5) != 0 && licznik < 3000);

            if (licznik >= 3000) {
                return;
            }

            GameObjects.add(ryba);
            GameObjects.get(GameObjects.size() - 1).setX(x);
            GameObjects.get(GameObjects.size() - 1).setY(y);
            LICZBA_RYB++;
        }

    }

    public void animationLoop() {
        long startTime = System.currentTimeMillis(); //rejestruje czas w ktorym zaczyna sie gra
        long currTime = startTime;

        while (czyDziala) {

            long elapsedTime = System.currentTimeMillis() - currTime;
            currTime = currTime + elapsedTime;
//aktualizacja duszków
            //clearGameObjects();
            update(elapsedTime);
            genObiekt();
//narysowanie i aktualizacja ekranu
            Graphics2D g = screen.getGraphics();
            draw(g);
            //drawFade(g, currTime - startTime);
            g.dispose();
            screen.update();

            try {
                Thread.sleep(20);
            } catch (InterruptedException ex) {

            }

        }
        czyDziala = true;

    }

    static Point PktA = new Point(1, 1);
    static Point PktB = new Point(1, 1);
    static Point PktGracza = new Point(1, 1);

    public synchronized void update(long elapsedTime) {

        for (int i = 0; i < GameObjects.size(); i++) {
            AnimObject ob = GameObjects.get(i);
            if (ob instanceof Player) {
                PktGracza.setLocation(player.getX(), player.getY());
                PktA.setLocation(pktMyszki.getX(), player.getY());
                PktB.setLocation(player.getX(), pktMyszki.getY());
                double dx = (double) (odcinek(PktGracza, PktA) / 1000);
                double dy = (double) (odcinek(PktGracza, PktB) / 1000);

                if (pktMyszki.getX() > player.getX()) {
                    player.setVelocityX((float) dx);
                } else {
                    player.setVelocityX((float) -dx);
                }

                if (pktMyszki.getY() > player.getY()) {
                    player.setVelocityY((float) dy);
                } else {
                    player.setVelocityY((float) -dy);

                }
                //System.out.println("dx:" + dx);
                // System.out.println("dy:" + dy);
                //System.out.println("myszkaX:" + pktMyszki.getX());
                //System.out.println("myszkaY:" + pktMyszki.getY());
                // System.out.println("PktGraczaX:" + PktGracza.getX());
                // System.out.println("PktGraczaY:" + PktGracza.getY());
                player.update(elapsedTime);
            } else if (ob instanceof Fish) {
                //zmiana predkosci na przeciwne gdy dojdą duszki do krawedzi

                int contact = ((Fish) ob).FishConntact(player);
                System.out.println("point:" + ((Fish) ob).getPoint());
                if (contact != 0) {
                    if ((ob instanceof NiebieskaRyba)) {
                        wygrana();
                    }
                }
                
                
                if (contact == -1) {
                    ob.delete();

                    if ((ob instanceof Rozdymka)) {
                        ((Player) GameObjects.get(0)).grow(-0.5);
                    } else {
                        double size_player = ((Fish) GameObjects.get(0)).getSize();
                        double size_ob = ((Fish) ob).getSize();
                        ((Player) GameObjects.get(0)).grow(0.05);
                    }
                    ((Player) GameObjects.get(0)).addPoints(((Fish) ob).getPoint());

                    GameObjects.remove(i);
                    LICZBA_RYB--;

                } else if (contact > 0) {
                    porazka();
                }

                if (ob.getX() < 0) {
                    ob.setVelocityX(Math.abs(ob.getVelocityX()));
                } else if (ob.getX() + ob.getWidth() >= screen.getWidth()) {
                    ob.setVelocityX(-Math.abs(ob.getVelocityX()));
                }
                if (ob.getY() < 0) {
                    ob.setVelocityY(Math.abs(ob.getVelocityY()));
                } else if (ob.getY() + ob.getHeight() >= screen.getHeight()) {
                    ob.setVelocityY(-Math.abs(ob.getVelocityY()));
                }
                //aktualizacja pozycji na podstawie czasu ktory uplynal razy predkosc duszka
                ob.update(elapsedTime);
            }
        }

    }

    public void porazka() {
        player.delete();

        czyDziala = false;

        double wielkosc = ((Fish) GameObjects.get(0)).getSize();
        String w_s = "";
        if (wielkosc < 0.5) {
            w_s = "Mała ryba";
        } else if (wielkosc < 1) {
            w_s = "Srednia ryba";
        } else if (wielkosc < 1.2) {
            w_s = "Dorosla ryba";
        } else if (wielkosc < 1.5) {
            w_s = "Wielka ryba";
        } else if (wielkosc < 2) {
            w_s = "Ogromna ryba";
        } else if (wielkosc < 2.5) {
            w_s = "Gigantyczna Ryba";
        } else if (wielkosc > 3) {
            w_s = "Niesowicie wielka Ryba";
        } else {
            w_s = "";
        }

        int punkty = ((Player) GameObjects.get(0)).getPoints();
        String w = w_s;
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Wyniki(w, punkty).setVisible(true);
            }
        });
        // JOptionPane.showMessageDialog(null, "Przegrales: \n Zostales zjedzony przez wieksza rybe !!! \nUzyskales wielkosc: " + w_s + "\nPunkty:" + punkty + "/1000", "Przegrana", JOptionPane.INFORMATION_MESSAGE);
        GameObjects.remove(0);
    }

    public void wygrana() {
        player.delete();

        czyDziala = false;

        double wielkosc = ((Fish) GameObjects.get(0)).getSize();
        String w_s = "";
        if (wielkosc < 0.5) {
            w_s = "Mała ryba";
        } else if (wielkosc < 1) {
            w_s = "Srednia ryba";
        } else if (wielkosc < 1.2) {
            w_s = "Dorosla ryba";
        } else if (wielkosc < 1.5) {
            w_s = "Wielka ryba";
        } else if (wielkosc < 2) {
            w_s = "Ogromna ryba";
        } else if (wielkosc < 2.5) {
            w_s = "Gigantyczna Ryba";
        } else if (wielkosc > 3) {
            w_s = "Niesowicie wielka Ryba";
        } else {
            w_s = "";
        }

        int punkty = ((Player) GameObjects.get(0)).getPoints();
        String w = w_s;
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new WynikiSukces(w, punkty).setVisible(true);
            }
        });
        // JOptionPane.showMessageDialog(null, "Przegrales: \n Zostales zjedzony przez wieksza rybe !!! \nUzyskales wielkosc: " + w_s + "\nPunkty:" + punkty + "/1000", "Przegrana", JOptionPane.INFORMATION_MESSAGE);
        GameObjects.remove(0);
    }

    public synchronized void draw(Graphics2D g) {
        g.drawImage(bgImage, 0, 0, null);

        AffineTransform transform = new AffineTransform();
        Iterator<AnimObject> s = GameObjects.iterator();
        while (s.hasNext()) {
            AnimObject ob = s.next();
            if (ob instanceof Fish) {
                Fish r = (Fish) ob;
                transform.setToTranslation(r.getX(), r.getY());
                //transform.s

                if (r.getVelocityX() < 0) {
                    transform.scale(-r.getSize(), r.getSize());
                    transform.translate(-r.getWidth(), 0);
                } else if (r.getVelocityX() >= 0) {
                    transform.scale(r.getSize(), r.getSize());
                }

                g.drawImage(r.getImage(), transform, null);
            }

        }

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {
        pktMyszki.setLocation(e.getX(), e.getY());
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public static double odcinek(int x1, int y1, int x2, int y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

    public static double odcinek(Point p, Point p2) {
        return Math.sqrt(Math.pow(p.getX() - p2.getX(), 2) + Math.pow(p.getY() - p2.getY(), 2));
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            czyDziala = false;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public Image loadImage(String nazwa) {
        return new ImageIcon(nazwa).getImage();
    }

}
