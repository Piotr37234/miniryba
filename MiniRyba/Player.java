/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MiniRyba;

/**
 *
 * @author użytkownik
 */
public class Player extends Fish {

    public Player(Animation anim, float x, float y, float dx, float dy) {
        super(anim, x, y, dx, dy);
        this.size = 0.4;
        this.punkty = 0;
        this.isLive = true;
    }
    void grow(double size1) {
        size = size + size1;
    }

    void addPoints(int points) {
        punkty = punkty + points;
    }

    int getPoints() {
        return punkty;
    }

}
