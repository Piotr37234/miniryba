package MiniRyba;

import java.awt.Image;

public class AnimObject {

    Animation anim;
    float x;   // poloznie ryby x
    float y;  //poloznie ryby y
    float dx;  // wektor porusznia x
    float dy;   // wektor porusznia y 
    String name;
    boolean isLive;

    //Tworzy nowego duszka wraz z przypisaną animacją
    public AnimObject(Animation anim, float x, float y, float dx, float dy) {
        this.anim = anim;
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
        this.isLive = true;
    }

    //aktualizuje obiekt animation dla bieżacego Sprite oraz 
    //swoja pozycje na podstawie predkosci
    public void update(long elapsedTime) {
        x = x + dx * elapsedTime;
        y = y + dy * elapsedTime;
        anim.update(elapsedTime);
    }

    //zwraca współżędną x duszka
    public float getX() {
        return x;
    }

    public String getName() {
        return name;
    }

    //zwraca współżędną y duszka
    public float getY() {
        return y;
    }

    //ustawia współżędną x duszka
    public void setX(float x) {
        this.x = x;
    }

    //ustawia współżędną y duszka
    public void setY(float y) {
        this.y = y;
    }

    //zwraca szekosc duszka korzystajac z przypisanego obiektu animacji
    public int getWidth() {
        return anim.getImage().getWidth(null);
    }

    //zwraca wysokosc duszka korzystajac z przypisanego obiektu animacji
    public int getHeight() {
        return anim.getImage().getHeight(null);
    }

    //zwraca predkosc w poziomie duszka w pixelach na milisekunde
    public float getVelocityX() {
        return dx;
    }

    //zwraca predkosc w pionie duszka w pixelach na milisekunde
    public float getVelocityY() {
        return dy;
    }

    //ustawia predkosc w poziomie duszka w pixelach na milisekunde
    public void setVelocityX(float dx) {
        this.dx = dx;
    }

    //ustawia predkosc w pionie duszka w pixelach na milisekunde
    public void setVelocityY(float dy) {
        this.dy = dy;
    }

    //zwraca biezacy rysunek dla tego obiektu duszka
    public Image getImage() {
        return anim.getImage();
    }

    boolean Ifconntact(AnimObject ob, double s, double s1) {
        
        //boolean test_x = Math.abs(ob.getX()-getX())<(getWidth()+ob.getWidth()) ;       
        // boolean test_y = Math.abs(ob.getY()-getY())<(getHeight()+ob.getHeight()) ;  
        boolean test_x;
        boolean test_y;

        if (getX() > ob.getX()) {
            test_x = (getX() - ob.getX()) < (double) (ob.getWidth() * s1);
        } else {
            test_x = (ob.getX() - getX()) < (double) (getWidth() * s);
        }

        if (getY() > ob.getY()) {
            test_y = (getY() - ob.getY()) < (double) (ob.getHeight() * s1);
        } else {
            test_y = (ob.getY() - getY()) < (double) (getHeight() * s);
        }
        //System.out.println("width:" + getWidth() * s + " height:" + getHeight() * s);
        boolean wynik = test_x && test_y;
        return wynik;
    }

    boolean isLive() {
        return isLive;
    }

    void delete() {
        isLive = false;
    }
}
