/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MiniRyba;

/**
 *
 * @author użytkownik
 */
public class Fish extends AnimObject {

    double size;
    int punkty;
    boolean isLive;

    public Fish(Animation anim, float x, float y, float dx, float dy, double size, int punkty) {
        super(anim, x, y, dx, dy);
        this.size = size;
        this.punkty = punkty;
        this.isLive = true;
     
    }

    public Fish(Animation anim, float x, float y, float dx, float dy) {
        super(anim, x, y, dx, dy);
    }

    Double getSize() {
        return size;
    }


   public int getWidth() {
       //System.out.println("ok");
       return (int)(anim.getImage().getWidth(null));
        //return (int)(anim.getImage().getWidth(null))+1;
    }
   
    public int getHeight() {
       // System.out.println("ok1");
       //return (int)(anim.getImage().getHeight(null));
        return (int)(anim.getImage().getHeight(null));
    }

    int FishConntact(Fish fish) {
        if (Ifconntact(fish,getSize(),fish.getSize())) {
            
            if (getSize() > fish.getSize()) {
                fish.delete();
                return 1;
            } else {
                delete();
                return -1;
            }
            
        }
        return 0;
    }
    
     int FishConntactTestCreate(Fish fish,double free) {
        if (Ifconntact(fish,getSize()+free,fish.getSize()+free)) {
            if (getSize() > fish.getSize()) {
                fish.delete();
                return 1;
            } else {
                delete();
                return -1;
            }
        }
        return 0;
    }
    

    int getPoint() {
        return punkty;
    }

}
