
package MiniRyba;

import java.awt.Image;
import java.util.ArrayList;

/**
 *
 * @author Piotr and Krzysztof
 */
public class Animation {

    private ArrayList frames;
    private int currFrameIndex;
    private long animTime;
    private long totalDuration;

    
    //tworzy nowy pusty obiekt Animation
    public Animation() {
        frames = new ArrayList(); //lista kletek animacji 
        totalDuration = 0;
        start();
    }
    
    //Dodaje do animacji rysunek o określonym czasie wyswietlania
    public synchronized void addFrame(Image image,long duration){
        totalDuration += duration;
        frames.add(new AnimFrame(image,totalDuration)); // dodaje kololjne obrazy 
        
    }
    
    //uruchamia animacje od poczatku
    public synchronized void start(){
        animTime = 0;
        currFrameIndex = 0;
    }
    
    
    //w razie potrzeby modyfikuje biężacą ramkę (rysunek) animcji
    public synchronized void update(long elapsedTime){
        if(frames.size()>1){
            animTime += elapsedTime;
            
            if(animTime>= totalDuration){
                animTime = animTime % totalDuration;
                currFrameIndex=0;
            }
            while(animTime > getFrame(currFrameIndex).endTime){
              currFrameIndex++;  
             //koniec czasu 1 klatki to nastepna
            }
            
        }
    }
    
    
    //pobiera bieżący rysunek z animacji jesli nie ma
    //zwraca null
    public synchronized Image getImage(){
        if(frames.size()==0){
        return null;
    }
        else{
            return getFrame(currFrameIndex).image;
        }
        
    }
    
    private AnimFrame getFrame(int i){
        return (AnimFrame)frames.get(i);
    }
    
    
    
    
    

}
