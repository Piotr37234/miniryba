
package MiniRyba;

import java.awt.DisplayMode;
import java.awt.EventQueue;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;
import javax.swing.JFrame;

/**
 *
 * @author Piotr and Krzysztof
 */
public class ScreenManager {

    private GraphicsDevice device;

    ScreenManager() {
        GraphicsEnvironment environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        device = environment.getDefaultScreenDevice();
    }

    public DisplayMode getCurrentDisplayMode() {
        return device.getDisplayMode();
    }

    public DisplayMode[] getCompatibleDisplayModes() {
        return device.getDisplayModes();
    }

    public void setFullScreen(DisplayMode displayMode,MouseMotionListener mouseMotion,MouseListener mouseButton,KeyListener keylistener) {
        final JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setUndecorated(true);
        frame.setIgnoreRepaint(true);
        frame.setResizable(false);
        frame.addMouseMotionListener(mouseMotion);
        frame.addMouseListener(mouseButton);
        frame.addKeyListener(keylistener);
        
        device.setFullScreenWindow(frame);

        if (displayMode != null
                && device.isDisplayChangeSupported()) {

            try {
                device.setDisplayMode(displayMode);
            } catch (IllegalArgumentException ex) {
                frame.setSize(displayMode.getWidth(), displayMode.getHeight());
            }

            try {
                EventQueue.invokeAndWait(new Runnable() {
                    public void run() {
                        frame.createBufferStrategy(2);
                    }
                });
            } catch (InterruptedException ex) {

            } catch (InvocationTargetException ex) {

            }
        }

    }

    public Graphics2D getGraphics() {
        Window window = device.getFullScreenWindow();
        if (window != null) {
            BufferStrategy strategy = window.getBufferStrategy();
            return (Graphics2D) strategy.getDrawGraphics();
        } else {
            return null;
        }
    }

    public void update() {
        Window window = device.getFullScreenWindow();

        if (window != null) {
            BufferStrategy strategy = window.getBufferStrategy();
            if (!strategy.contentsLost()) {
                strategy.show();
            }
        }

    }

    public JFrame getFullScreenWindow() {
        return (JFrame) device.getFullScreenWindow();
    }

    public int getHeight() {
        Window window = device.getFullScreenWindow();

        if (window != null) {
            return window.getHeight();
        } else {
            return 0;
        }

    }

    public int getWidth() {
        Window window = device.getFullScreenWindow();

        if (window != null) {
            return window.getWidth();
        } else {
            return 0;
        }

    }

    public void restoreScreen() {

        Window window = device.getFullScreenWindow();
        if (window != null) {
            window.dispose();
        }
        device.setFullScreenWindow(null);
    }

    public BufferedImage createCompatibleImage(int w, int h, int transparency) {
        Window window = device.getFullScreenWindow();
        if (window != null) {
            GraphicsConfiguration gc = window.getGraphicsConfiguration();
            return gc.createCompatibleImage(w, h, transparency);
        }
        return null;
    }

}
